# coding=utf-8

from __future__ import unicode_literals

from setuptools import setup

setup(name='steam_email_parser',
      version='0.1',
      description='Smple library for parsing email for steam codes',
      url='https://gitlab.com/fogstream/steam_email_parser',
      author='Nikita "CryptoManiac" Sivakov',
      author_email='cryptomaniac.512@gmail.com',
      license='ANY',
      packages=['steam_email_parser'],
      install_requires=[
          'future',
          'six',
      ],
      zip_safe=False)

