# coding=utf-8

from __future__ import unicode_literals


class EmailSearchTimeout(Exception):
    """Raised if email search aborted by tieout"""

    def __init__(self, timeout):
        """
        :param timeout: timeout that was used for search control
        :type timeout: int
        """
        super(EmailSearchTimeout, self).__init__()
        self.timeout = timeout

    def __str__(self):
        message = 'Email search was aborted by timeout {} sec.'.format(
            self.timeout)
        return message
