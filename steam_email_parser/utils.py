# coding=utf-8

from __future__ import unicode_literals

from future.utils import raise_with_traceback

import email
import imaplib
import re
import time

from .exceptions import EmailSearchTimeout


def get_email_text_bock(msg):
    """iterate over email bocks.

    :param msg: some email message
    :type msg: email.message.Messag
    :return: yield payloads
    :rtype: generator

    """
    if msg.is_multipart():
        for part in msg.get_payload():
            if not part.is_multipart():
                yield part.get_payload()
    else:
        yield msg.get_payload()


def search_email_with_timeout(connection, search_params, timeout):
    """Recursively search email by params with timeout.

    :param connection: connection to mail server
    :type connection: imaplib.IMAP4_SSL
    :param search_params: parameters for search emails
    :type search_params: str
    :param timeout: time in seconds, where search abort
    :type timeout: int
    :return: latest mail uid
    :rtype: bytes

    """
    def _search():
        _result, _data = connection.uid('search', None, search_params)
        uids = _data[0]
        uid_list = uids.split()
        return uid_list

    freq = 5
    time_left = timeout
    while time_left > 0:
        uids_list = _search()
        if not uids_list:
            time_left -= freq
            time.sleep(freq)
        else:
            latest_email_uid = uids_list[-1]
            return latest_email_uid

    raise_with_traceback(EmailSearchTimeout(timeout))


def get_email_code(host, user, password, timeout=60):
    """Get email code from latest mail.

    :param host: email server host address
    :type host: str
    :param user: user for connection
    :type user: str
    :param password: password of current user for connection
    :type password: str
    :param timeout: time in seconds while function will search email
    :type timeout: int
    :return: code from email
    :rtype: str

    """
    # do login
    conn = imaplib.IMAP4_SSL(host)
    conn.login(user, password)
    conn.select('inbox')

    search_params = [
        'UNSEEN',
        'FROM "steam support"',
        'HEADER Subject "access from new web or mobile device"',
        'BODY "steam guard code you need to login"'
    ]
    search_str = ' '.join(search_params)

    # get latest mail uids by search params
    latest_email_uid = search_email_with_timeout(conn, search_str, timeout)

    # get latest mail
    _result, _data = conn.uid('fetch', latest_email_uid, 'RFC822')
    raw_email = _data[0][1]
    # TODO: python 2 only. Do this compatible
    message = email.message_from_string(raw_email)

    for text in get_email_text_bock(message):
        _code = re.findall(r'\r\n\r\n(.{5,5})\r\n\r\n\r\n', text)
        if _code:
            break

    code = _code[0]
    return code
